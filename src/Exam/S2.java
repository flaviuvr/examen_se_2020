package Exam;

public class S2 extends Thread {
    S2(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 13; i++) {
            System.out.println(getName() + " message number: " + i);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        S2 t1 = new S2("Othread1");
        S2 t2 = new S2("Othread2");

        t1.start();
        t2.start();

    }
}
