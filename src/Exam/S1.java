package Exam;

public class S1 {
    public static void main(String[] args) {
    }
}

class B implements C{
    private long t;

    public void metD(D d){}
    public void x(){}
}

interface C{}

class D{}

class E extends G{
    private F f;
    private H h;
    private B b;

    public E(H h){
        this.h = h;
        this.f = new F();
    }

    public void metG(int i){}
}

class F{
    public void metA(){}
}

class G{}

class H{}

